import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import API.ISTSManager;
import model.vo.*;
import model.vo.VOPlan.ParadaPlanVO;
import model.vo.VOPlan.ParadaPlanVO.RutaPlanVO.ViajePlanVO;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.data_structures.Stack;
import model.exceptions.StopNotFoundException;
import model.exceptions.StopTimeNotFoundException;
import model.exceptions.TripNotFoundException;

public class STSManager implements ISTSManager {

	private DoubleLinkedList<VORuta> routes;
	private DoubleLinkedList<VOStopTime> stopTimes;
	private DoubleLinkedList<VOAgency> agencies;
	private DoubleLinkedList<VOEspecialService> especialServices;
	private DoubleLinkedList<VOService> services;
	private DoubleLinkedList<VOTransbordo> transfers;
	private DoubleLinkedList<VOShape> shapes;
	private DoubleLinkedList<VOInfo> feed_info;

	private RingList<VOViaje> trips;
	private RingList<VOParada> stops;

	private Queue<VOBusUpdate> updates;
	private DoubleLinkedList<VORouteEstimate> estimate;
	private PersistenceManager persistencia;

	@Override
	public void ITSInit() 
	{

		routes = new DoubleLinkedList<VORuta>();
		stopTimes = new DoubleLinkedList<VOStopTime>();
		agencies = new DoubleLinkedList<VOAgency>();
		especialServices = new DoubleLinkedList<VOEspecialService>();
		feed_info = new DoubleLinkedList<VOInfo>();
		services = new DoubleLinkedList<VOService>();
		shapes = new DoubleLinkedList<VOShape>();
		stops = new RingList<VOParada>();
		transfers = new DoubleLinkedList<VOTransbordo>();
		trips = new RingList<VOViaje>();

	}

	@Override
	public void ITScargarGTFS() 
	{

		persistencia = new PersistenceManager();


		routes = persistencia.loadRoutes("./data/routes.txt");



		agencies = persistencia.loadAgencies("./data/agency.txt");

		especialServices = persistencia.loadEspecialServices("./data/calendar_dates.txt");

		feed_info = persistencia.loadInfo("./data/feed_info.txt");

		services = persistencia.loadServices("./data/calendar.txt");

		shapes = persistencia.loadShapes("./data/shapes.txt");

		stops = persistencia.loadStops("./data/stops.txt");

		transfers = persistencia.loadTransfers("./data/transfers.txt");

		trips = persistencia.loadTrips("./data/trips.txt");

		stopTimes = persistencia.loadStopTimes("./data/stop_times.txt");

		System.out.println("Se completo la carga");

	}

	@Override
	public void ITScargarTR(String fecha) throws FileNotFoundException 
	{
		String day = fecha.substring(fecha.length()-2, fecha.length());
		String month = fecha.substring(fecha.length()-3, fecha.length()-2);
		String fecha1 = month +"-"+ day;
		String file = "./data/RealTime-"+month+"-"+day+"-BUSES_SERVICE";
		estimate = persistencia.readStopsEstimateService("./data/RealTime-"+ fecha1 +"-STOPS_ESTIM_SERVICES");
		updates = persistencia.readBusUpdate(file);


	}

	@Override
	public IList<VORuta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha) throws FileNotFoundException
	{
		ITScargarTR(fecha);

		Iterator<VORuta> iterador = routes.iterator();
		DoubleLinkedList<VORuta> rutasEmpresa= new DoubleLinkedList<VORuta>();
		while (iterador.hasNext())
		{
			boolean encontro = false;
			Queue<VOBusUpdate> temp = updates;
			VORuta ruta = iterador.next();
			if(ruta.getAgencyId().equals(nombreEmpresa))
			{
				while (!temp.isEmpty() && !encontro)
				{
					VOBusUpdate update = temp.dequeue();
					if(update.getRouteNo().equals(ruta.getRouteShortName()))
					{
						encontro = true;
						rutasEmpresa.addAtEnd(ruta);
					}
				}
			}

		}

		return rutasEmpresa;
	}

	@Override
	public IList<VOViaje> ITSviajesRetrasadosRuta(String idRuta, String fecha) throws TripNotFoundException, Exception 
	{
		ITScargarTR(fecha);
		DoubleLinkedList<VOViaje> lista = new DoubleLinkedList<VOViaje>();

		while(!updates.isEmpty()) 
		{ 	
			VOBusUpdate temp = updates.dequeue();

			if (temp.getRouteNo().equals(idRuta)) 
			{
				VOParada stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());		
				VOStopTime stopTime = stopTimeById(stop.getStopId(), temp.getTripId());

				Date estimateDate = parsingDate(stopTime.getDepartureTime());
				Date realDate = parsingDate(temp.getRecordedTime());

				long diference = realDate.getTime() -  (estimateDate.getTime() + 60000);
				if(diference > 0) 
				{
					lista.addInOrder(tripById(temp.getTripId()));
				}

			}

		}

		return lista;
	}

	@Override
	public IList<VOParada> ITSparadasRetrasadasFecha(String fecha) throws Exception 
	{
		ITScargarTR(fecha);
		DoubleLinkedList<VOParada> lista = new DoubleLinkedList<VOParada>();
		DoubleLinkedList<VORetardo> listaRetrasos = new DoubleLinkedList<VORetardo>();

		while(!updates.isEmpty()) 
		{ 	
			VOBusUpdate temp = updates.dequeue();


			VOParada stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());		
			VOStopTime stopTime = stopTimeById(stop.getStopId(), temp.getTripId());

			Date estimateDate = parsingDate(stopTime.getDepartureTime());
			Date realDate = parsingDate(temp.getRecordedTime());

			long diference = realDate.getTime() -  (estimateDate.getTime() + 60000);
			if(diference > 0) 
			{
				stop.setNumeroIncidentes(stop.getNumeroIncidentes()+1);

				VORetardo atraso = new VORetardo(stop, temp.getTripId(), stop.getStopId(), (int)diference);
				VORetardo buscado = listaRetrasos.findElement(atraso);


				if(buscado == null) 
				{
					listaRetrasos.addInOrder(atraso);
				}


			}

		}


		Iterator<VORetardo> iterador = listaRetrasos.iterator();

		while(iterador.hasNext()) 
		{
			lista.addAtEnd(iterador.next().getStop());

		}

		return lista;
	}

	@Override
	public IList<VOTransfer> ITStransbordosRuta(String idRuta, String fecha) throws Exception
	{
		ITScargarTR(fecha);
		Stack<VOParada>  paradas = new Stack<VOParada>();
		DoubleLinkedList<VOTransfer> lista = new DoubleLinkedList<VOTransfer>();
		while(!updates.isEmpty()) 
		{ 	VOBusUpdate temp = updates.dequeue();
		if(temp.getRouteNo().equals(idRuta))
		{
			VOParada parada = stopByCoordinates(temp.getLatitude(), temp.getLongitude());
			paradas.push(parada);

			Iterator<VOTransbordo> iterador = transfers.iterator();
			while(iterador.hasNext()) 
			{
				VOTransbordo transbordo = iterador.next();
				if(transbordo.getFrom_stop_id() == parada.getStopId()) 
				{

					VOTransfer transfer = new VOTransfer(transbordo.getMin_transfer_time());
					transfer.getListadeParadas().add(parada);
					transfer.getListadeParadas().add(paradaPorId(transbordo.getTo_stop_id()));

					if(!lista.existElement(transfer)) 
					{
						lista.addAtEnd(transfer);
					}

				}

			}

		}
		}

		return lista;
	}

	@Override
	public VOPlan ITSrutasPlanUtilizacion(IList<String> idsDeParadas, String fecha, String horaInicio, String horaFin) throws Exception 
	{
		VOPlan plan =  new VOPlan();

		Queue<VOBusUpdate> listaUpdates = updates;
		Queue<VOBusUpdate> listaUpdates2 = updates;
		ITScargarTR(fecha);

		Iterator<String> iteradorIDS = idsDeParadas.iterator();

		while(iteradorIDS.hasNext())
		{
			String actual = iteradorIDS.next() ;
			int paradaActualid = Integer.parseInt(actual) ;
			VOParada stop = stopByID(paradaActualid);

			VOPlan.ParadaPlanVO trip = plan.new ParadaPlanVO(actual, stop.getStopLat(), stop.getStopLon());

			trip.asignarRutasPorParada(listaUpdates2, horaInicio, horaFin);
			if(trip.getRutasAsociadasAParada().getElement() != null) 
			{
				plan.getSecuenciaDeParadas().add(trip);
			}


		}
		return null;
	}


	//TODO estudiante b



	@Override
	public IList<VORuta> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha)
	{
		Iterator<VOAgency> iteradorA = agencies.iterator();
		DoubleLinkedList<VORuta> rutasEmpresa= new DoubleLinkedList<VORuta>();
		while (iteradorA.hasNext())
		{
			VOAgency agency = iteradorA.next();
			if(agency.getName().equals(nombreEmpresa))
			{
				Iterator<VORuta> iteradorR = routes.iterator();

				while (iteradorR.hasNext())
				{
					VORuta ruta = iteradorR.next();
					if(ruta.getAgencyId().equals(nombreEmpresa))
					{

						rutasEmpresa.addAtEnd(ruta);
					}

				}
			}

		}
		return  rutasEmpresa;
	}

	@Override
	public IList<VOViaje> ITSviajesRetrasoTotalRuta(String idRuta, String fecha) throws Exception
	{
		try {
			ITScargarTR(fecha);

		}
		catch (Exception e) {
			System.out.println("No se encontraron datos para " + fecha );
		}
		DoubleLinkedList<VOViaje> lista = new DoubleLinkedList<VOViaje>();

		while(!updates.isEmpty()) 
		{ 	
			VOBusUpdate temp = updates.dequeue();

			if (temp.getRouteNo().equals(idRuta)) 
			{
				VOParada stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());		
				VOStopTime stopTime = stopTimeById(stop.getStopId(), temp.getTripId());

				Date estimateDate = parsingDate(stopTime.getDepartureTime());
				Date realDate = parsingDate(temp.getRecordedTime());

				long diference = realDate.getTime() -  (estimateDate.getTime() + 60000);
				if(diference > 0) 
				{
					lista.addInOrder(tripById(temp.getTripId()));
				}

			}

		}

		return lista;
	}

	@Override
	public VORangoHora ITSretardoHoraRuta(String idRuta, String fecha) throws Exception
	{
		try {
			ITScargarTR(fecha);

		}
		catch (Exception e) {
			System.out.println("No se encontraron datos para " + fecha );
		}
		DoubleLinkedList<VORangoHora> r= new DoubleLinkedList<VORangoHora>();


		while(!updates.isEmpty()) 
		{ 	
			VOBusUpdate temp = updates.dequeue();

			if (temp.getRouteNo().equals(idRuta)) 
			{
				VOParada stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());		
				VOStopTime stopTime = stopTimeById(stop.getStopId(), temp.getTripId());

				Date estimateDate = parsingDate(stopTime.getDepartureTime());
				Date realDate = parsingDate(temp.getRecordedTime());

				long diference = realDate.getTime() -  (estimateDate.getTime() + 60000);
				if(diference > 0) 
				{
					int h= estimateDate.getHours();

					VORetardoViaje rv= new VORetardoViaje(temp.getTripId());
					rv.adicionart(diference);
					VORangoHora rh= new VORangoHora(h);
					VORangoHora rr=r.findElement(rh);

					if(rr!=null) {

						rr.addRV(rv);
					}
					else {
						rh.addRV(rv);
						r.add(rh);

					}

				}

			}

		}
		VORangoHora mayor=null;
		double ma=0;

		Iterator<VORangoHora> i= r.iterator();
		while(i.hasNext()) {
			VORangoHora m=i.next();
			if(m.totalRetardo()>ma) {
				mayor=m;
				ma=m.totalRetardo();

			}
		}
		return mayor;

	}

	@Override
	public IList<VOViaje> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) 
	{

		int origen= Integer.parseInt(idOrigen);
		int destino=Integer.parseInt(idDestino);
		try {
			ITScargarTR(fecha);

		}
		catch (Exception e) {
			System.out.println("No se encontraron datos para " + fecha );
		}

		DoubleLinkedList<VOViaje> temp= new DoubleLinkedList<VOViaje>();

		DoubleLinkedList<VOViaje> rTrips= new DoubleLinkedList<VOViaje>();

		Iterator<VOViaje> iTrip = trips.iterator();

		while (iTrip.hasNext())
		{
			VOViaje trip = iTrip.next();
			Iterator<VOStopTime> iSTime= stopTimes.iterator();

			while(iSTime.hasNext()) {
				VOStopTime sTime= iSTime.next();
				if(sTime.getTripId()==trip.id()) {
					if(sTime.getStopId()==origen) {
						if(sTime.getArrivalTime().compareTo(horaInicio)>0 && sTime.getArrivalTime().compareTo(horaFin)<0) {
							try {
								temp.add(trip);
							} catch (Exception e) {

							}
						}
					}

				}

			}
		}

		Iterator<VOViaje> iTemp = temp.iterator();

		while(iTemp.hasNext()) {
			VOViaje trip = iTrip.next();
			Iterator<VOStopTime> iSTime= stopTimes.iterator();
			while(iSTime.hasNext()) {
				VOStopTime sTime= iSTime.next();
				if(trip.id()==sTime.getTripId()) {
					if(sTime.getStopId()==destino) {
						try {
							rTrips.add(trip);
						} catch (Exception e) {

						}
					}
				}

			}

		}



		return rTrips;
	}

	@Override
	public VORuta ITSrutaMenorRetardo(String fecha) throws Exception  
	{
		try {
			ITScargarTR(fecha);		
		}
		catch (Exception e) {
			System.out.println("No se encontraron datos para " + fecha );
		}

		DoubleLinkedList<VORetardoRuta> lRR= new DoubleLinkedList<VORetardoRuta>();


		while(!updates.isEmpty()) 
		{ 	


			VOBusUpdate temp = updates.dequeue();


			VOParada stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());		
			VOStopTime stopTime = stopTimeById(stop.getStopId(), temp.getTripId());

			Date estimateDate = parsingDate(stopTime.getDepartureTime());
			Date realDate = parsingDate(temp.getRecordedTime());

			long diference = realDate.getTime() -  (estimateDate.getTime() + 60000);
			if(diference > 0) 
			{
				VORetardoRuta ret = new VORetardoRuta(Integer.parseInt(temp.getRouteNo()));
				VORetardoRuta rrr=lRR.findElement(ret);
				VOShape vShape=getShape(temp.getTripId());
				if(rrr==null) {
					ret.adicionarDistancia(vShape.getDist_traveled());
					ret.adicionarRetardo(diference);
					lRR.add(ret);
				}
				else {
					rrr.adicionarRetardo(diference);
					rrr.adicionarDistancia(vShape.getDist_traveled());
				}

			}

		}
		VORetardoRuta menor= null;
		double r= Double.MAX_VALUE;
		Iterator<VORetardoRuta> iRR= lRR.iterator();
		while(iRR.hasNext()) {
			VORetardoRuta rr= iRR.next();
			if(rr.darRetardoPromedio()<r) {
				r=rr.darRetardoPromedio();
				menor=rr;
			}
		}
		VORuta rRu= new VORuta(menor.rutaId(), "", "","", "0", 0, "", 0, "");
		rRu=routes.findElement(rRu);
		return rRu;



	}

	@Override
	public IList<VOServicio> ITSserviciosMayorDistancia(String fecha) throws Exception
	{
		ITScargarTR(fecha);
		DoubleLinkedList<VOBusUpdate> lista = listaFromCola(updates);
		DoubleLinkedList<VOServicio> listaServicios = new DoubleLinkedList<VOServicio>();
		Iterator<VOBusUpdate> listaIterador = lista.iterator();
		while(listaIterador.hasNext()) 
		{
			VOBusUpdate update = listaIterador.next();
			VOServicio servicio = new VOServicio(Integer.toString(update.getTripId()), 0);

			if(!listaServicios.existElement(servicio)) 
			{
				Iterator<VOBusUpdate> listaIterador2 = lista.iterator();
				double distanciaLON = 0;
				double distanciaLAT = 0;
				while(listaIterador2.hasNext()) 
				{
					VOBusUpdate update2 = listaIterador.next();
					if(update2.getTripId() == Integer.parseInt(servicio.getServiceId()))
						if(servicio.getDistanciaRecorrida() == 0) 
						{
							distanciaLON = update2.getLongitude();
							distanciaLAT = update2.getLatitude();
						}
						else 
						{
							double distancia = getDistance(distanciaLAT, distanciaLON, update2.getLatitude(), update2.getLongitude());
							distanciaLON = update2.getLongitude();
							distanciaLAT = update2.getLatitude();
							servicio.setDistanciaRecorrida(servicio.getDistanciaRecorrida() + (int) distancia);
						}

				}
				
				listaServicios.addInOrder(servicio);
			}
		}


		return listaServicios;
	}

	@Override
	public IList<VORetardo> ITSretardosViaje(String fecha, String idViaje) throws Exception 
	{
		ITScargarTR(fecha);
		DoubleLinkedList<VORetardo> lista = new DoubleLinkedList<VORetardo>();

		while(!updates.isEmpty()) 
		{ 	
			VOBusUpdate temp = updates.dequeue();

			if (temp.getTripId() == Integer.parseInt(idViaje)) 
			{
				VOParada stop = stopByCoordinates(temp.getLatitude(), temp.getLongitude());		
				VOStopTime stopTime = stopTimeById(stop.getStopId(), temp.getTripId());

				Date estimateDate = parsingDate(stopTime.getDepartureTime());
				Date realDate = parsingDate(temp.getRecordedTime());

				long diference = realDate.getTime() -  (estimateDate.getTime() + 60000);
				if(diference > 0) 
				{
					VORetardo retardo = new VORetardo(null, Integer.parseInt(idViaje), stop.getStopId(), (int) diference );
					lista.addAtEnd(retardo);

				}


			}

		}
		return lista;
	}
	@Override
	public IList<VOParada> ITSparadasCompartidas(String fecha) throws FileNotFoundException, StopNotFoundException
	{
		ITScargarTR(fecha);
		DoubleLinkedList<VOParada> lista = new DoubleLinkedList<VOParada>();


		Iterator<VORouteEstimate> iterador = estimate.iterator();
		while(iterador.hasNext()) 
		{
			VORouteEstimate actual = iterador.next();
			boolean encontro = false;
			Iterator<VORouteEstimate> iterador2 = estimate.iterator();
			while(iterador2.hasNext() && !encontro) 
			{


				VORouteEstimate actual2 = iterador2.next();
				if(actual.getStopId() == actual2.getStopId() && actual.getRouteNo() != actual.getRouteNo()) 
				{

					VOParada stop = paradaPorId(actual2.getStopId());
					encontro = true;

					VOParada buscado = lista.findElement(stop);


					if(buscado == null) 
					{
						lista.addAtEnd(stop);
					}

				}


			}

		}

		return lista;

	}


	public Stack<VOParada> listStops(Integer tripID) 
	{
		Stack<VOParada> pila = new Stack<VOParada>();
		Queue<VOBusUpdate> update = updates;

		VOBusUpdate actual = null;
		while (!update.isEmpty())
		{
			actual = update.dequeue();
			if(actual.getTripId() == tripID)
			{
				Iterator<VOParada> iterator = stops.iterator();
				while(iterator.hasNext())
				{
					VOParada actualStop = iterator.next();
					Double distance = getDistance(actual.getLatitude(), actual.getLongitude(), actualStop.getStopLat(), actual.getLongitude());

					if(distance <= 70)
					{
						pila.push(actualStop);
					}
				}
			}
		}
		return pila;
	}





	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;


	}

	public DoubleLinkedList<VOBusUpdate> listaFromCola(Queue<VOBusUpdate> cola)
	{
		DoubleLinkedList<VOBusUpdate> lista = new DoubleLinkedList<VOBusUpdate>();
		while(!cola.isEmpty()) 
		{
			lista.addAtEnd(cola.dequeue());
		}
		return lista;

	}

	private Double toRad(Double value)
	{
		int f = 180;
		return value * Math.PI / f;

	}

	public VOStopTime stopTimeById(int stopId, int tripId) throws StopTimeNotFoundException 
	{
		VOStopTime stopTime = null;
		boolean find = false;
		Iterator<VOStopTime> timeIterator = stopTimes.iterator();
		while (timeIterator.hasNext() && !find) 
		{
			VOStopTime estimateTime = timeIterator.next();
			if(estimateTime.getStopId() == stopId && estimateTime.getTripId() == tripId) 
			{
				stopTime = estimateTime;
				find = true;

			}
		}

		if(stopTime == null) 
		{
			throw new StopTimeNotFoundException();
		}

		return stopTime;
	}

	public VOViaje tripById(int tripId) throws TripNotFoundException 
	{
		VOViaje trip = null;
		boolean find = false;
		Iterator<VOViaje> tripsIterator = trips.iterator();
		while (tripsIterator.hasNext() && !find) 
		{
			VOViaje tripTemp = tripsIterator.next();
			if(tripTemp.getIdViaje() == tripId ) 
			{
				trip = tripTemp;
				find = true;

			}
		}

		if(trip == null) 
		{
			throw new TripNotFoundException();
		}

		return trip;
	}


	public VOParada stopByCoordinates(Double latitude, Double longitude) throws StopNotFoundException 
	{
		VOParada stop= null;
		boolean find = false;
		Iterator<VOParada> iterator = stops.iterator();
		while(iterator.hasNext() && !find)
		{
			VOParada actualStop = iterator.next();
			Double distance = getDistance(latitude, longitude, actualStop.getStopLat(), actualStop.getStopLon());
			if(distance <= 70)
			{
				stop = actualStop;
				find = true;
			}
		}

		if(stop == null) 
		{
			throw new StopNotFoundException();
		}
		return stop;
	}

	public VOParada stopByID(int id) throws StopNotFoundException 
	{
		VOParada stop= null;
		boolean find = false;
		Iterator<VOParada> iterator = stops.iterator();
		while(iterator.hasNext() && !find)
		{
			VOParada actualStop = iterator.next();

			if(actualStop.getStopId() == id)
			{
				stop = actualStop;
				find = true;
			}
		}

		if(stop == null) 
		{
			throw new StopNotFoundException();
		}
		return stop;
	}

	public VOParada paradaPorId(int id) throws StopNotFoundException 
	{
		VOParada stop= null;

		boolean find = false;
		Iterator<VOParada> iterator = stops.iterator();
		while(iterator.hasNext() && !find)
		{
			VOParada actualStop = iterator.next();

			if(actualStop.getStopId() ==  id)
			{
				stop = actualStop;
				find = true;
			}
		}

		if(stop == null) 
		{
			throw new StopNotFoundException();
		}
		return stop;
	}



	public Date parsingDate(String date) throws ParseException 
	{	
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss a");
		DateFormat formatMilitar = new SimpleDateFormat( "HH:mm:ss");
		Date time = null;

		try 
		{
			time = format.parse(date);
		} catch (ParseException e) {

			time = formatMilitar.parse(date);	 
		}

		if (time != null) 
		{
			String formattedDate = formatMilitar.format(time);
		}

		return time;

	}

	public VOShape getShape(int tripId) {
		VOViaje v=null;
		try {
			v = tripById(tripId);
		} catch (TripNotFoundException e1) {

		}
		VOShape r= null;
		boolean e= false;
		Iterator<VOShape> iShape= shapes.iterator();
		while(iShape.hasNext()&& !e) {
			VOShape shape = iShape.next();
			if(v.getShapeId()==shape.getId()) {
				e=true;
				r=shape;
			}
		}


		return r;


	}






}