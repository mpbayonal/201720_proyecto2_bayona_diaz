package model.data_structures;

import model.data_structures.T23Node.T;

public class T23 <K extends Comparable<K>,V>{
	
	T23Node<K,V> root;
	T23Node<K,V> ground;
	int size;
	
	public T23() {
		ground=null;
		root=null;
		size=0;
	}
	public T23(T23Node<K,V> node) {
		ground=null;
		root=node;
		size=1;
	}
	public V get(K key) {
		V r= null;
		if(root!=null) {
			r=get(key, root);
		}
		return r;
	}
	
	public V get(K key, T23Node<K,V> node) {
		V r= node.get(key);
			if(r==null){
				int side =node.whatSide(key);
				if(side==-1) {
					r=get(key,node.getRight());
				}
				else if(side==1) {
					r=get(key, node.getRight());
				}
				else if( side==0) {
					r=get(key, node.getCenter());
				}
			} 
		
		return r;	
	}
	public void add(K key,V value) {
		

		T23NodeS nodeS = new T23NodeS(key,value);
		if(root==null) {
			T23Node node= new T23Node(nodeS);
			root=node;
			size++;
		}
		else {
			add(ground,root,nodeS);
		}
	

	}
	public void add(T23Node padre,T23Node node, T23NodeS nodeS ) {
		if(node.leave()) {
			if(node.getTipo()==T.Uno) {
				node.setDos(nodeS);
				size++;
			}
			else if(node.getTipo()==T.Dos) {
				node.setTres(nodeS);
				size++;
			}
		}
		else {
			int c= node.whatSide(nodeS.getKey());
			if(c==-1) {
				add(node,node.getLeft(),nodeS);
				
			}
			else if(c==0) {
				add(node,node.getCenter(),nodeS);
				
			}
			else if(c==1) {
				add(node, node.getRight(), nodeS);
			}
		}
		if(node.getTipo()==T.Tres) {
			split(padre,node);
		}
		if(ground!=null) {
			root=ground;
			ground=null;
		}
		
	}
	public void split(T23Node<K,V> padre, T23Node<K,V> node) {
		
		T23Node<K,V> n= new T23Node(node.getTres());
		
		if(!node.leave()) {
			if(padre.whichSon(node)==1) {
				
				padre.setLeft(n);
			}
			else if(padre.whichSon(node)==2) {
				padre.setCenter(n);
			}
			else if(padre.whichSon(node)==3) {
				padre.setRight(node);
			}
		}

	}
	public void delete(K key) {
		if(!root.delete(key)) {
			int c= root.whatSide(key);
			if(c==1) {
				delete(root,root.getRight(), key);
			}
			else if( c==0) {
				delete(root,root.getCenter(), key);
			}
			else if( c==-1) {
				delete(root,root.getLeft(), key);
			}
			
		}
		
	}
	public void delete(T23Node<K,V> padre, T23Node<K,V> node, K key) {
		if(node.getTipo()==T.Uno) {
			T23Node<K,V> n= new T23Node(node.getLeft().getUno());
			if(node.delete(key)) {
			if(padre.whichSon(node)==1) {
				padre.setLeft(n);
			}
			else if(padre.whichSon(node)==2) {
				padre.setCenter(n);
			}
			else if(padre.whichSon(node)==3) {
				padre.setRight(node);
			}
			}
			
		}
		else if( node.getTipo()==T.Dos) {
			T23Node<K,V> n=n= new T23Node(node.getCenter().getUno());
			if(node.delete(key)) {
				node.changeUno(node.getCenter().getUno());
			}
		}
	}
	public int getSize() {
		return size;
	}
}
