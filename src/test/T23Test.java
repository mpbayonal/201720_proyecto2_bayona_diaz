package test;

import junit.framework.TestCase;
import model.data_structures.T23;
import model.data_structures.T23Node;
import model.data_structures.T23NodeS;

public class T23Test extends TestCase {
	
	T23<Integer, Integer> tree;
	
	public void setUp1() {
		tree= new T23<Integer, Integer>();
	}
	public void setUp2() {
		T23NodeS<Integer, Integer> n=new T23NodeS(1,1);
		T23Node m= new T23Node(n);
		tree=new T23<Integer,Integer>(m);		
	}
	
	public void testGet() {
		setUp1();
		for (int i = 0; i < 10; i++) {
			tree.add(i, 1*2);
		}
		assertEquals("el elemento no es correto", 2, tree.get(1));
		assertEquals("el elemento no es correto", 4, tree.get(2));
		
	}
	public void testDelete() {
		setUp1();
		for (int i = 0; i < 10; i++) {
			tree.add(i, 1*2);
		}
		tree.delete(1);
		tree.delete(2);
		assertEquals("el elemento no es correto", null, tree.get(1));
		assertEquals("el elemento no es correto", null, tree.get(2));
		
		
	}
	public void testAdd() {
		setUp1();
		for (int i = 0; i < 10; i++) {
			tree.add(i, 1*2);
		}
		
		assertEquals("el elemento no es correto", 5, tree.getSize());
		setUp2();
		for (int i = 0; i < 10; i++) {
			tree.add(i, 1*2);
		}
		assertEquals("el elemento no es correto", 6, tree.getSize());
	}

}
