package model.data_structures;

public class HashNode<K extends Comparable<K>,V> {

	 K key;
	 V value;
	 HashNode<K, V> next;
	 HashNode<K, V> anterior;
	
	 public HashNode() {
		 this.value=null;
		 this.key=null;
	 }
	public HashNode(K key, V value)
    {
        this.key = key;
        this.value = value;
        next = null;
        anterior = null;
    }

	public K darLlave() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

	public HashNode<K, V> getNext() {
		return next;
	}


	public void setNext(HashNode<K, V> next) {
		this.next = next;
	}

	public HashNode<K, V> getAnterior() {
		return anterior;
	}

	public void setAnterior(HashNode<K, V> anterior) {
		this.anterior = anterior;
	}

	public void setNull() {
		this.key=null;
		this.value=null;
	}
}

