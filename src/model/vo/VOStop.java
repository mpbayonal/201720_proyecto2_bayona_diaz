package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop implements Comparable<VOStop>{

    // -----------------------------------------------------------------
    // Attributes
    // -----------------------------------------------------------------

	 private int stopId;
	    private Integer stopCode;
	    private String stopName;
	    private String stopDesc;
	    private double stopLat;
	    private double stopLon;
	    private String zoneId;
	    private String stopUrl;
	    private Integer locationType;
	    private String parentStation;

	    // -----------------------------------------------------------------
	    // Constructor
	    // -----------------------------------------------------------------

	    public VOStop(int pStopId, Integer pStopCode, String pStopName, String pStopDesc, double pStopLat, double pStopLon, String pZoneId, String pStopUrl, Integer pLocationType, String pParentStation)
	    {
	        stopId = pStopId;
	        stopCode = pStopCode;
	        stopName = pStopName;
	        stopDesc = pStopDesc;
	        stopLat = pStopLat;
	        stopLon = pStopLon;
	        zoneId = pZoneId;
	        stopUrl = pStopUrl;
	        locationType = pLocationType;
	        parentStation = pParentStation;
	    }

	    // -----------------------------------------------------------------
	    // Methods - Getters and Setters
	    // -----------------------------------------------------------------

	    /**
	     * @return id - Stop's id
	     */
	    public int id() {

	        return stopId;
	    }

	    /**
	     * @return name - Stop name
	     */
	    public String getName() {

	        return stopName;
	    }

	    public int getStopId() {
	        return stopId;
	    }

	    public void setStopId(int stopId) {
	        this.stopId = stopId;
	    }

	    public Integer getStopCode() {
	        return stopCode;
	    }

	    public void setStopCode(Integer stopCode) {
	        this.stopCode = stopCode;
	    }

	    public String getStopName() {
	        return stopName;
	    }

	    public void setStopName(String stopName) {
	        this.stopName = stopName;
	    }

	    public String getStopDesc() {
	        return stopDesc;
	    }

	    public void setStopDesc(String stopDesc) {
	        this.stopDesc = stopDesc;
	    }

	    public double getStopLat() {
	        return stopLat;
	    }

	    public void setStopLat(double stopLat) {
	        this.stopLat = stopLat;
	    }

	    public double getStopLon() {
	        return stopLon;
	    }

	    public void setStopLon(double stopLon) {
	        this.stopLon = stopLon;
	    }

	    public String getZoneId() {
	        return zoneId;
	    }

	    public void setZoneId(String zoneId) {
	        this.zoneId = zoneId;
	    }

	    public String getStopUrl() {
	        return stopUrl;
	    }

	    public void setStopUrl(String stopUrl) {
	        this.stopUrl = stopUrl;
	    }

	    public Integer getLocationType() {
	        return locationType;
	    }

	    public void setLocationType(Integer locationType) {
	        this.locationType = locationType;
	    }

	    public String getParentStation() {
	        return parentStation;
	    }

	    public void setParentStation(String parentStation) {
	        this.parentStation = parentStation;
	    }

	    // -----------------------------------------------------------------
	    // Methods - Logic
	    // -----------------------------------------------------------------

	    @Override
	    public int compareTo(VOStop o)
	    {
	        int r;

	        if(o.id() == stopId) {
	            r=0;
	        }
	        else if(o.id() < stopId) {
	            r=1;
	        }
	        else {
	            r=-1;
	        }

	        return r;
	    }

}
