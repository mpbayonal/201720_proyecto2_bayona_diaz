package model.data_structures;


import java.util.ArrayList;



public class LPHashTable<K extends Comparable<K>,V>  {
	/*Cosntantes*/

	private static final int CAPACIDAD = 11;
	/*Atributos*/

	private int m;
	private int size;
	private ArrayList<HashNode<K,V>> array;

	/**Crea una nueva LPHashTable**/
	public LPHashTable() {
		this(CAPACIDAD);
	}
	/**Crea una nueva LPHashTable
	 * @param pM la capacidad inicial
	 **/
	public LPHashTable(int pm) {
		array = new ArrayList<>();
		m = pm;
		size = 0;
		HashNode<K,V> n= new HashNode();

		for (int i = 0; i < m; i++) {
			array.add(n);
		}		
	}

	/** Inserta un nuevo elemento en la LPHT
	 * @param key la llave del objeto
	 * @param value el objeto
	 * @post queda agrado el objeto**/
	public void put(K key, V value) {
		if (over())
		{
			rehash();
		}

		int i;
		for (i = getIndex(key);array.get(i).key!=null; i = (i + 1) % m) {
			if(array.get(i).key.equals(key)) {
				array.get(i).setValue(value);
				return;
			}
		}

		HashNode<K, V> newNode = new HashNode<K, V>(key, value);
		array.set(i, newNode);
		size++;


	}

	public  V  get(K key) 
	{

		int i;
		for (i = getIndex(key);array.get(i).key!=null; i = (i + 1) % m) {
			if(array.get(i).key.equals(key)) {
				return array.get(i).getValue();

			}
		}


		return null;
	}
	public boolean delete(K key) {

		if(!contains(key)) {
			return false;
		}



		for (int i = getIndex(key);array.get(i).key!=null; i = (i + 1) % m) {
			if(array.get(i).key.equals(key)) {
				array.get(i).setNull();
				size--;
				return true;

			}
		}
		return false;

	}
	public int size() {
		return size;
	}
	public int getM() {
		return m;
	}
	public double sizeOverM() {
		return (1.0*size)/m;
	}

	public int getIndex( K key) {

		int hashCode = key.hashCode();
		int index = (hashCode & 0x7fffffff) % m;
		return index;


	}
	public boolean isEmpty() { 
		return size == 0; 
	}
	public boolean over() {
		boolean r= false;
		if((1.0*size)/m > 0.75){
			r=true;
		}
		return r;
	}
	public void rehash() {
		HashNode<K,V> n= new HashNode();
		ArrayList<HashNode<K, V>> temp = array;
		array = new ArrayList<>();
		m = darPrimoSiguiente(2*m);
		size = 0;
		for (int j = 0; j < m; j++) {
			array.add(n);
		}
		for (HashNode<K, V> node : temp)
		{
			if(node.key!=null) {
				this.put(node.key, node.value);
			}
		}
	}
	public boolean contains(K key) {		       
		return get(key) != null;
	}

	public int darPrimoSiguiente(int i) {
		int r=i;

		while(true) {
			r++;
			if(esPrimo(r)) {
				break;
			}
		}

		return r;
	}
	public boolean esPrimo(int n) {
		for(int i=2;i<n;i++) {
			if(n%i==0)
				return false;
		}
		return true;
	}

	public Iterable<K> llaves() {
		QueueIterable<K> cola = new QueueIterable<K>();
		for (int i = 0; i < array.size(); i++) 
		{

			cola.enqueue(array.get(i).darLlave());


		}
		return cola;
	}
}


