package model.data_structures;

import java.util.Iterator;

public class QueueIterable<V> implements Iterable<V>
{
	private NodoListaDoblementeEncadenada<V> primero;
	private NodoListaDoblementeEncadenada<V> ultimo;
	private int size;



	public QueueIterable() 
	{

		primero = null;
		ultimo = null;
		size = 0;
	}


	public void enqueue(V item) 
	{
		NodoListaDoblementeEncadenada<V> nodo = new NodoListaDoblementeEncadenada<V>(item);
		if (size == 0) 
		{
			primero = nodo;
			ultimo = nodo;
			size++;
		}
		else 
		{
			NodoListaDoblementeEncadenada<V> ultimotemp = ultimo;
			ultimotemp.cambiarAnterior(nodo);
			nodo.cambiarSiguiente(ultimotemp);
			ultimo = nodo;
			size++;
		}
	}


	public V getElement() 
	{
		if(isEmpty()) 
		{
			return null;
		}
		else {
			return primero.darElemento();
		}
	}



	public V dequeue() 
	{
		V elemento = null;
		if(size == 1) 
		{
			elemento = primero.darElemento();
			primero = null;
			ultimo = null;
			size--;

		}
		else if (size > 0) 
		{
			elemento = primero.darElemento();
			NodoListaDoblementeEncadenada<V> temp = primero.darAnterior();
			temp.cambiarSiguiente(null);
			primero = temp;
			size--;

		}


		return elemento;
	}

	public boolean isEmpty() 
	{
		return size == 0;
	}
	public int getSize()
	{
		return size;
	}




	class iterador implements Iterator<V>{


		public boolean hasNext() {
			return isEmpty();
		}

		public V next() {
			return dequeue();
		}
	}


	@Override
	public Iterator<V> iterator() {

		return new iterador() {
		};
	}

}
