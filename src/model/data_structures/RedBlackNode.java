package model.data_structures;

public class RedBlackNode<K extends Comparable<K>, V> 
{
	private K llave;
	private V valor;
	private RedBlackNode<K, V> derecho;
	private RedBlackNode<K, V> izquierdo;
	private int size;
	private boolean color;
	
	public RedBlackNode(K llave, V valor,int size,
			boolean color) {
		super();
		this.llave = llave;
		this.valor = valor;
		this.derecho = null;
		this.izquierdo = null;
		this.size = size;
		this.color = color;
	}

	public K getLlave() {
		return llave;
	}

	public void setLlave(K llave) {
		this.llave = llave;
	}

	public V getValor() {
		return valor;
	}

	public void setValor(V valor) {
		this.valor = valor;
	}

	public RedBlackNode<K, V> getDerecho() {
		return derecho;
	}

	public void setDerecho(RedBlackNode<K, V> derecho) {
		this.derecho = derecho;
	}

	public RedBlackNode<K, V> getIzquierdo() {
		return izquierdo;
	}

	public void setIzquierdo(RedBlackNode<K, V> izquierdo) {
		this.izquierdo = izquierdo;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isColor() {
		return color;
	}

	public void setColor(boolean color) {
		this.color = color;
	}
	

}
