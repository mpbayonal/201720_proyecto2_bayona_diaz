package model.data_structures;

import java.util.Iterator;

import com.sun.scenario.effect.Merge;



public class DoubleLinkedList<T extends Comparable<T> > 

{

	private NodoListaDoblementeEncadenada<T> nodoActual;
	private NodoListaDoblementeEncadenada<T> primerNodo;
	private NodoListaDoblementeEncadenada<T> ultimoNodo;
	private int size;

	/**
	 * Construye una lista doble
	 */
	public DoubleLinkedList() 
	{
		nodoActual = new NodoListaDoblementeEncadenada<T>(null);
		primerNodo = nodoActual;
		ultimoNodo = nodoActual;
		size = 0;
	}

	/**
	 * Construye una lista doble con un solo nodo
	 */

	public DoubleLinkedList(NodoListaDoblementeEncadenada<T> nodo) 
	{
		nodoActual = nodo;	
		primerNodo = nodoActual;
		ultimoNodo = nodoActual;
		size = 1;
	}

	public void doMergeSort() 
	{
		NodoListaDoblementeEncadenada<T>temp = primerNodo;
		temp = mergeSort(temp);
		primerNodo = temp;
		nodoActual = primerNodo;

	}

	public void doSelectionSort() 
	{  	NodoListaDoblementeEncadenada<T>temp = primerNodo;
	temp = selectionSort(temp);
	primerNodo = temp;
	nodoActual = primerNodo;

	}

	public void doInsertionSort() 
	{
		NodoListaDoblementeEncadenada<T>temp = primerNodo;
		temp = insertionSort(temp);
		primerNodo = temp;
		nodoActual = primerNodo;

	}
	
	public void doShellSort() 
	{
		NodoListaDoblementeEncadenada<T>temp = primerNodo;
		temp = mergeSort(temp);
		primerNodo = temp;
		nodoActual = primerNodo;
	}
	
	

	public NodoListaDoblementeEncadenada<T> insertionSort(NodoListaDoblementeEncadenada<T> nodo)
	{
		NodoListaDoblementeEncadenada<T> temp = nodo.darSiguiente();
		NodoListaDoblementeEncadenada<T> temp1 = null;
		NodoListaDoblementeEncadenada<T> nodoReturn = null;
		while(temp != null) 
		{
			temp1 = temp;
			NodoListaDoblementeEncadenada<T> temp5 = temp.darSiguiente();
			while(temp1.darAnterior() != null) 
			{
				NodoListaDoblementeEncadenada<T> temp3 = temp1.darAnterior();

				if(temp1.darElemento().compareTo(temp1.darAnterior().darElemento()) < 0) 
				{

					if(temp.darSiguiente() != null) 
					{
						temp1.darSiguiente().cambiarAnterior(temp3);
					}

					if(temp3.darAnterior() != null) 
					{
						temp3.darAnterior().cambiarSiguiente(temp1);
					}


					temp3.cambiarSiguiente(temp1.darSiguiente());
					temp1.cambiarAnterior(temp3.darAnterior());
					temp1.cambiarSiguiente(temp3);
					temp3.cambiarAnterior(temp1);
					if(temp1.darAnterior() == null) 
					{
						nodoReturn = temp1;
					}

					temp1 = temp3;

				}
				temp1 = temp1.darAnterior();

			}

			temp = temp5;
		}

		return nodoReturn;

	}

	public NodoListaDoblementeEncadenada<T> selectionSort(NodoListaDoblementeEncadenada<T> nodo) 
	{
		NodoListaDoblementeEncadenada<T> primero = null;
		NodoListaDoblementeEncadenada<T> temp = nodo;
		NodoListaDoblementeEncadenada<T> temp2 = nodo;
		NodoListaDoblementeEncadenada<T> smaller = nodo;
		for (int i = 0; i < size-1; i++) 
		{	temp2 = temp.darSiguiente();

		smaller = temp;
		while(temp2 != null) 
		{		
			if(smaller.darElemento().compareTo(temp2.darElemento()) > 0) 
			{
				smaller = temp2;
			}
			temp2 = temp2.darSiguiente();

		}

		if(smaller != temp) 
		{
			NodoListaDoblementeEncadenada<T> aux = smaller.darSiguiente();
			NodoListaDoblementeEncadenada<T> aux2 = smaller.darAnterior();

			if(i == 0) 
			{



				if(smaller.darSiguiente() == null) 
				{
					smaller.cambiarAnterior(null);
					temp.darSiguiente().cambiarAnterior(smaller);
					smaller.cambiarSiguiente(temp.darSiguiente());

					temp.cambiarAnterior(aux2);
					temp.cambiarSiguiente(null);
					temp.darAnterior().cambiarSiguiente(temp);


				}
				else 
				{
					smaller.cambiarAnterior(null);
					temp.darSiguiente().cambiarAnterior(smaller);
					smaller.cambiarSiguiente(temp.darSiguiente());

					temp.cambiarAnterior(aux2);
					temp.cambiarSiguiente(aux);
					temp.darAnterior().cambiarSiguiente(temp);
					temp.darSiguiente().cambiarAnterior(temp);
				}
			}
			else if (i == size-2) 
			{

				temp.darAnterior().cambiarSiguiente(smaller);
				smaller.cambiarAnterior(temp.darAnterior());
				temp.darSiguiente().cambiarAnterior(smaller);
				smaller.cambiarSiguiente(temp.darSiguiente());

				temp.cambiarAnterior(aux2);
				temp.cambiarSiguiente(null);
				temp.darAnterior().cambiarSiguiente(temp);




			}
			else 
			{
				if(smaller.darSiguiente() == null) 
				{
					temp.darAnterior().cambiarSiguiente(smaller);
					smaller.cambiarAnterior(temp.darAnterior());
					temp.darSiguiente().cambiarAnterior(smaller);
					smaller.cambiarSiguiente(temp.darSiguiente());

					temp.cambiarAnterior(aux2);
					temp.cambiarSiguiente(null);
					temp.darAnterior().cambiarSiguiente(temp);

				}
				else 
				{
					temp.darAnterior().cambiarSiguiente(smaller);
					smaller.cambiarAnterior(temp.darAnterior());
					temp.darSiguiente().cambiarAnterior(smaller);
					smaller.cambiarSiguiente(temp.darSiguiente());

					temp.cambiarAnterior(aux2);
					temp.cambiarSiguiente(aux);
					temp.darAnterior().cambiarSiguiente(temp);
					temp.darSiguiente().cambiarAnterior(temp);
				}
			}



		}


		if(i == 0) 
		{
			primero = smaller;
		}

		temp = smaller.darSiguiente();


		}

		return primero;

	}


	/**
	 * Devuelve un iterador sobre la lista;
	 */
	public Iterator<T> iterator() {

		Iterator<T> iterador = new DoubleLinkedListIterator();

		return iterador;
	}

	/**
	 * Devuelve el tamaño de la lista;
	 */
	public Integer getSize() {
		return size;
	}

	public NodoListaDoblementeEncadenada<T> mergeSort(NodoListaDoblementeEncadenada<T> nodo) 
	{
		if (nodo == null || nodo.darSiguiente() == null) 
		{
			return nodo;

		}
		else 
		{
			NodoListaDoblementeEncadenada<T> segundo = split(nodo);
			nodo = mergeSort(nodo);
			segundo = mergeSort(segundo);

			return merge(nodo, segundo);

		}

	}

	public NodoListaDoblementeEncadenada<T> merge(NodoListaDoblementeEncadenada<T> primero , NodoListaDoblementeEncadenada<T> segundo)
	{
		NodoListaDoblementeEncadenada<T> temp1 = primero;
		NodoListaDoblementeEncadenada<T> temp2 = segundo;
		NodoListaDoblementeEncadenada<T> nodotemp = null;

		NodoListaDoblementeEncadenada<T> nodo = null;
		if(primero == null) 
		{
			return segundo;
		}
		else if (segundo == null) 
		{
			return primero;
		}
		else {

			if(temp1.darElemento().compareTo(temp2.darElemento()) < 0) 
			{
				nodo = temp1;
				temp1 = temp1.darSiguiente();
				nodotemp = nodo;
			}
			else 
			{	
				nodo = temp2;
				temp2 = temp2.darSiguiente();
				nodotemp = nodo;

			}

			while(temp1 != null || temp2 != null) 
			{

				if(temp1 == null) 
				{
					nodotemp.cambiarSiguiente(temp2);
					temp2.cambiarAnterior(nodo);
					break;
				}
				else if (temp2 == null) 
				{
					nodotemp.cambiarSiguiente(temp1);
					temp1.cambiarAnterior(nodo);
					break;
				}
				else {
					if(temp1.darElemento().compareTo(temp2.darElemento()) < 0) 
					{
						nodotemp.cambiarSiguiente(temp1); 
						temp1 = temp1.darSiguiente();
						nodotemp.darSiguiente().cambiarAnterior(nodotemp);
						nodotemp.darSiguiente().cambiarSiguiente(null);
						nodotemp = nodotemp.darSiguiente();
					}
					else 
					{
						nodotemp.cambiarSiguiente(temp2); 
						temp2 = temp2.darSiguiente();
						nodotemp.darSiguiente().cambiarAnterior(nodotemp);
						nodotemp.darSiguiente().cambiarSiguiente(null);
						nodotemp = nodotemp.darSiguiente();
					}
				}

			}
		}

		return nodo;

	}

	public NodoListaDoblementeEncadenada<T> split(NodoListaDoblementeEncadenada<T> first)
	{
		NodoListaDoblementeEncadenada<T> temp = first;
		NodoListaDoblementeEncadenada<T> mitad = first;
		NodoListaDoblementeEncadenada<T> segundo = null;
		int i = 1;		
		while(temp.darSiguiente() != null) 
		{
			temp = temp.darSiguiente();
			if(i % 2 == 0 && i != 2) 
			{
				mitad = mitad.darSiguiente();
			}
			i++;
		}


		segundo = mitad.darSiguiente();
		segundo.cambiarAnterior(null);
		mitad.cambiarSiguiente(null);

		return segundo;
	}



	/**
	 * cambia la referencia del nodo actual por el siguiente.
	 * @throws Exception 
	 */
	public void next() throws Exception 
	{
		if(nodoActual == null && size != 0)
		{
			nodoActual = getNodoAtK(0);
		}
		else
		{
			nodoActual = nodoActual.darSiguiente();
		}


	}

	/**
	 * cambia la referencia del nodo actual por el anterior.
	 * @throws Exception 
	 */

	public void previous() throws Exception {

		if(nodoActual == null && size != 0)
		{
			nodoActual = getNodoAtK(size-1);
		}
		else
		{
			nodoActual = nodoActual.darAnterior();
		}

	}

	/**
	 * devuelve el elemento del nodo actual
	 */


	public T getElement() {

		return nodoActual.darElemento();
	}

	public boolean existElement(T element) {
		boolean existe = false;
		if(size != 0)
		{
			NodoListaDoblementeEncadenada<T> temp = primerNodo;
			int i = 0;
			while(i < size && !existe)
			{
				if(temp.darElemento().compareTo(element) == 0)
				{
					existe = true;
				}
				temp = temp.darSiguiente();
				i++;
			}
		}

		return existe;
	}

	public T findElement(T element) {
		T existe = null;
		if(size != 0)
		{
			NodoListaDoblementeEncadenada<T> temp = primerNodo;
			int i = 0;
			while(i < size )
			{
				if(temp.darElemento().compareTo(element) == 0)
				{
					existe = temp.darElemento();
				}
				temp = temp.darSiguiente();
				i++;
			}
		}

		return existe;
	}



	/**
	 * Devuelve el elemento del nodo que se encuentra en la posicion K
	 * @param k la posición del nodo del que se desea el elemento.
	 * @return el elemento  posición
	 * @throws Exception si pos < 0 o pos >= size() 
	 * @throws Exception si la lista esta vacia
	 */

	@Override
	public T getElementAtK(int k) throws Exception {
		T elemento = null;
		NodoListaDoblementeEncadenada<T> temp = primerNodo;
		if(primerNodo.darElemento() == null)
		{
			throw new Exception("La lista esta vacia");
		}
		else if( k > getSize() && k < 0)
		{
			throw new Exception("No existe el elemento K");
		}
		else
		{ int i = 0;
		while(i < k)
		{
			i++;

			temp = temp.darSiguiente();

		}

		elemento = temp.darElemento();

		}

		return elemento;
	}

	private NodoListaDoblementeEncadenada<T> getNodoAtK(int k) throws Exception {
		NodoListaDoblementeEncadenada<T> nodo = null;
		NodoListaDoblementeEncadenada<T> temp = primerNodo;
		if(nodoActual.darElemento() == null)
		{
			throw new Exception("La lista esta vacia");
		}
		else if( k > getSize() && k < 0)
		{
			throw new Exception("No existe el elemento K");
		}
		else
		{ int i = 0;
		while(i < k)
		{
			i++;
			nodo = temp;
			temp = temp.darSiguiente();
		}


		}

		return nodo;
	}

	/**
	 * agrega un nuevo nodo a la lista reemplazando al nodo actual como primer nodo.
	 * @param el elemento a añadir
	 * @return true si se añadio el elemento, false en caso contrario
	 * @throws Exception si el elemento es nulo
	 */

	public boolean add(T elementToAdd) throws Exception
	{
		boolean add = false;
		NodoListaDoblementeEncadenada<T> nodo = new NodoListaDoblementeEncadenada<T>(elementToAdd);
		if (elementToAdd == null)
		{
			throw new NullPointerException("el elemento es nulo");		
		}
		if(size == 0)
		{	
			primerNodo = nodo;
			ultimoNodo = nodo;
			nodoActual = nodo;
			add = true;
			size++;

		}
		else
		{

			NodoListaDoblementeEncadenada<T> temp = primerNodo;
			nodo.cambiarSiguiente(primerNodo);
			primerNodo.cambiarAnterior(nodo);
			size++;
			primerNodo = nodo;
			nodoActual = nodo;


			add = true;

		}


		return add;
	}

	/**
	 * agrega un nuevo nodo a la lista en orden
	 * @param el elemento a añadir
	 * @return true si se añadio el elemento, false en caso contrario
	 * @throws Exception si el elemento es nulo
	 */
	public boolean addInOrder(T elementToAdd) throws Exception
	{
		boolean add = false;
		if (elementToAdd == null)
		{
			throw new Exception("el elemento es nulo");
		}
		NodoListaDoblementeEncadenada<T> nuevo = new NodoListaDoblementeEncadenada<T>(elementToAdd);
		NodoListaDoblementeEncadenada<T> temp = primerNodo;
		if(getSize() == 0)
		{
			primerNodo = nuevo;
			ultimoNodo = nuevo;
			nodoActual = nuevo;
			size++;
		}
		else
		{ 
			int i = 0;

			while(temp.darSiguiente() != null && temp.darElemento().compareTo(elementToAdd) < 0)
			{
				i++;
				temp = temp.darSiguiente();
			}

			if(i == 0)
			{
				if(temp.darElemento().compareTo(elementToAdd) < 0)
				{
					temp.cambiarSiguiente(nuevo);
					nuevo.cambiarAnterior(temp);
					ultimoNodo = nuevo;
				}
				else
				{
					nuevo.cambiarSiguiente(temp);
					temp.cambiarAnterior(nuevo);
					primerNodo = nuevo;
					nodoActual = primerNodo;				
				}

				size++;
			}
			else if(i == size - 1)
			{
				if(ultimoNodo.darElemento().compareTo(elementToAdd) < 0)
				{
					ultimoNodo.cambiarSiguiente(nuevo);
					nuevo.cambiarAnterior(ultimoNodo);
					ultimoNodo = nuevo;
				}
				else
				{
					nuevo.cambiarSiguiente(ultimoNodo);
					ultimoNodo.cambiarAnterior(nuevo);
					nodoActual = nuevo;				
				}

				size++;
			}
			else
			{
				NodoListaDoblementeEncadenada<T> temp2 = temp.darAnterior();
				nuevo.cambiarSiguiente(temp);
				temp.cambiarAnterior(nuevo);
				nuevo.cambiarAnterior(temp2);
				temp2.cambiarSiguiente(nuevo);
				size++;
				add = true;
			}
		}

		return add;
	}

	/**
	 * agrega un nuevo nodo a la lista en la ultima posicion de la lista.
	 * @param el elemento a añadir
	 * @return true si se añadio el elemento, false en caso contrario
	 * @throws Exception si el elemento es nulo
	 */
	public boolean addAtEnd(T elementToAdd) throws NullPointerException
	{
		boolean add = false;
		if (elementToAdd == null)
		{
			throw new NullPointerException("el elemento es nulo");

		}
		NodoListaDoblementeEncadenada<T> temp = primerNodo;
		NodoListaDoblementeEncadenada<T> nuevo = new NodoListaDoblementeEncadenada<T>(elementToAdd);
		if(size == 0)
		{
			primerNodo = nuevo;
			ultimoNodo = nuevo;
			nodoActual = primerNodo;

			add = true;
			size++;
		}
		else
		{
			int i = 0;
			while(i < size-1 )
			{
				temp = temp.darSiguiente();

				i++;
			}


			nuevo.cambiarAnterior(temp);
			temp.cambiarSiguiente(nuevo);

			add = true;
			size++;
		}



		return add;

	}

	/**
	 * agrega un nuevo nodo en la posicion indicada por parametro
	 * @param elementToAdd el elemento a añadir
	 * @param k la posicion en la que se desa añadir el elemento
	 * @return true si se añadio el elemento, false en caso de que k este fuera de los parametos
	 * @throws Exception si el elemento es nulo
	 */

	public boolean addAtK(T elementToAdd, int k) throws Exception 
	{
		boolean add = false;
		if (elementToAdd == null)
		{
			throw new Exception("el elemento es nulo");
		}
		NodoListaDoblementeEncadenada<T> nuevo = new NodoListaDoblementeEncadenada<T>(elementToAdd);
		NodoListaDoblementeEncadenada<T> temp = primerNodo;
		if(getSize() == 0 && k == 0)
		{
			primerNodo = nuevo;
			ultimoNodo = nuevo;
			nodoActual = nuevo;
			size++;
		}
		else if(k == 0)
		{
			nuevo.cambiarSiguiente(temp);
			temp.cambiarAnterior(nuevo);
			primerNodo = nuevo;
			size++;
		}
		else if(k == size)
		{
			ultimoNodo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimoNodo);
			ultimoNodo = nuevo;
			size++;

		}

		else if( k > getSize()+1)
		{
			add = false;
		}

		else
		{ int i = 0;

		while(i < k)
		{
			i++;
			temp = temp.darSiguiente();
		}
		NodoListaDoblementeEncadenada<T> temp2 = temp.darAnterior();
		nuevo.cambiarSiguiente(temp);
		temp.cambiarAnterior(nuevo);
		nuevo.cambiarAnterior(temp2);
		temp2.cambiarSiguiente(nuevo);
		size++;
		add = true;
		}

		return add;
	}


	/**
	 * Elimina el nodo actual
	 * @return true si el elemento fue eliminado
	 * @return false si la lista esta vacia
	 */

	public boolean delete() 
	{
		boolean delete = true;

		if(size == 0)
		{
			delete = false;
		}

		else if (size == 1)
		{
			primerNodo = null;
			ultimoNodo = null;
			nodoActual = null;
			size--;
		}
		else
		{
			NodoListaDoblementeEncadenada<T>temp = primerNodo.darSiguiente();
			if(nodoActual == temp)
			{
				nodoActual = temp.darSiguiente();
			}
			primerNodo = primerNodo.darSiguiente();

			size--;


		}


		return delete;
	}

	/**
	 * elimina el nodo que tenga el mismo elemento que el dado por parametro
	 * @param elemento a eliminar.
	 * @return false si no se elimino el nodo
	 * @return true si se elimino el nodo
	 */

	public boolean deleteElement(T elementToDelete) 
	{

		boolean delete = false;
		NodoListaDoblementeEncadenada<T> temp = primerNodo;
		if(getSize() == 0)
		{
			delete = false;
		}
		else if(getElement().compareTo(elementToDelete) == 0 && size == 1)
		{
			delete = delete();
		}
		else
		{ 
			int i = 0;
			while(i < size  && !delete) 
			{
				if(elementToDelete.compareTo(temp.darElemento()) == 0)
				{
					NodoListaDoblementeEncadenada<T> temp1 = temp;
					if(nodoActual == temp)
					{
						nodoActual = temp.darSiguiente();
					}
					temp.darAnterior().cambiarSiguiente(temp.darSiguiente());
					temp.darSiguiente().cambiarAnterior(temp1.darAnterior());

					delete = true;
					size--;
				}
				else{
					i++;
					temp = temp.darSiguiente();
				}
			}


		}

		return delete;
	}

	/**
	 * elimina el nodo que se encuentra en la posicion K
	 * @param k la posición del nodo del que se desea el elemento.
	 * @return true si el elemento fue eliminado;
	 * @return false si el elemento no existe o la lista esta vacia;
	 */

	public boolean deleteAtK(int k) {
		boolean delete = true;
		NodoListaDoblementeEncadenada<T> temp = primerNodo;
		if(getSize() == 0)
		{
			delete = false;
		}
		else if( k > getSize())
		{
			delete = false;
		}
		else if(k == 0 && size == 1)
		{
			size--;
			primerNodo = null;
			nodoActual = null;

		}
		else
		{ 
			int i = 0;
			while(i < k)
			{
				i++;
				temp = temp.darSiguiente();
			}

			if(temp == nodoActual)
			{
				nodoActual = temp.darSiguiente();
			}

			if(temp.darSiguiente() != null) 
			{
				temp.darSiguiente().cambiarAnterior(temp.darAnterior());
			}
			else
			{
				ultimoNodo = temp.darAnterior();
			}

			if(temp.darAnterior() != null) 
			{
				temp.darAnterior().cambiarSiguiente(temp.darSiguiente());
			}
			else
			{
				primerNodo = temp.darSiguiente();
			}

			size--;

		}

		return delete;
	}

	public class DoubleLinkedListIterator implements Iterator<T>
	{
		NodoListaDoblementeEncadenada<T> anterior;
		NodoListaDoblementeEncadenada<T> siguiente;

		public DoubleLinkedListIterator() 
		{
			anterior = null;
			siguiente = nodoActual;
		}

		/**
		 * @return true si no se ha llegado al final de la lista;
		 * @return false si se llego al final de la lista;
		 */
		public boolean hasNext() 
		{
			return anterior.darSiguiente() != null ;
		}

		/**
		 * devuelve el elemento del siguiente nodo
		 */
		public T next() {

			anterior = siguiente;
			siguiente = siguiente.darSiguiente();


			return anterior.darElemento();
		}



	}

	




}
