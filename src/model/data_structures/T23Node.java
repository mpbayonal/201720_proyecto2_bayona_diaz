package model.data_structures;

public class T23Node<K, V> {
	public enum T{
		Uno,Dos,Tres
	}

	private T tipo;
	private T23NodeS uno;
	private T23NodeS dos;
	private T23NodeS tres;
	private T23Node<K,V> left;
	private T23Node<K,V> center;
	private T23Node<K,V> right;
	
	public T23Node(T23NodeS node) {
		tipo=T.Uno;
		uno=node;
		dos=null;
		tres=null;
		left=null;
		right=null;
		center=null;
		

	}
	public T23Node(T23NodeS node1, T23NodeS node2) {
		tipo=T.Dos;
		uno=node1;
		dos=node2;
		tres=null;
		left=null;
		right=null;
		center=null;
		
	}
	public void setDos(T23NodeS node) {
		tipo= T.Dos;
		dos=node;
	}
	public void setTres(T23NodeS node) {
		tipo= T.Tres;
		tres=node;
	}
	public void changeUno(T23NodeS node) {
		
		uno=node;
	}
	public void changeDos(T23NodeS node) {
		dos=node;
	}
	
	public void changeTres(T23NodeS node) {
		tres=node;
	}
	public T23NodeS deleteDos() {
		T23NodeS r=dos;
		tipo= T.Uno;
		dos=null;
		return r;
	}
	public T23NodeS deleteTres() {
		T23NodeS r=tres;
		tipo= T.Dos;
		tres=null;
		return r;
	}
	public T23NodeS getUno() {
		return uno;
	}
	public T23NodeS getDos() {
		return dos;
	}
	public T23NodeS getTres() {
		return tres;
	}
	public T23Node<K,V> getLeft(){
		return left;
	}
	public T23Node<K,V> getCenter(){
		return center;
	}
	public T23Node<K,V> getRight(){
		return right;
	}
	public void setLeft(T23Node<K,V> node) {
		left=node;
	}
	public void setRight(T23Node<K,V> node) {
		right=node;
	}
	public void setCenter(T23Node<K,V> node) {
		center=node;
	}
	public T getTipo() {
		return tipo;
	}
	public V get(K key) {
		V r=null;
		
		if(uno.getKey().compareTo(key)==0) {
			r=(V) uno.getValue();
		}
		else if(tipo== T.Dos ) {
			if(dos.getKey().compareTo(key)==0) {
				r= (V) dos.getValue();
			}
		}

		return r;
	}
	public boolean has(K key) {
		boolean r=false;
		if(uno.getKey().compareTo(key)==0) {
			r=true;
		}
		else if(tipo== T.Dos ) {
			if(dos.getKey().compareTo(key)==0) {
				r= true;
			}
		}
		return r;
	}
	public int whatSide(K key) {
		int r=2;
		if(tipo==T.Uno) {
			if(uno.getKey().compareTo(key)>0) {
				r=-1;
			}
			else {
				r=1;
			}
		}
		if(tipo==T.Dos) {
			if(uno.getKey().compareTo(key)>0) {
				r=-1;
			}
			else if(dos.getKey().compareTo(key)<0) {
				r=1;
			}
			else {
				r=0;
			}
		}
		return r;
		
	}
	public boolean leave() {
		if(left==null&&center==null&&right==null) {
			return true;
		}
		return false;
	}

	public int whichSon(T23Node<K,V> node) {
		int r=5;
		
		if(node.getLeft().getUno().getKey().compareTo(left.getUno().getKey())==0){
			r=1;
		}
		else if(node.getCenter().getUno().getKey().compareTo(center.getUno().getKey())==0) {
			r=2;
		}
		else if(node.getRight().getUno().getKey().compareTo(right.getUno().getKey())==0) {
			r=3;
		}
		
		return r;
	}
	public boolean delete(K key) {
		boolean r = false;
		if(key.equals(uno.getKey())) {
			uno = null;
			r=true;
		}
		if(key.equals(dos.getKey())) {
			dos = null;
			r=true;
		}

		
		
		return r;
	
	}
	
	
}
