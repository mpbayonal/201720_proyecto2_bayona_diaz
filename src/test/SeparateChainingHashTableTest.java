package test;

import junit.framework.TestCase;
import model.data_structures.SeparateChainingHashTable;

public class SeparateChainingHashTableTest extends TestCase {

	SeparateChainingHashTable<Integer, Double > lpht;
	
	public void setUp1() throws Exception {
		 lpht= new SeparateChainingHashTable<Integer,Double>();
		 for (int i = 0; i < 6; i++) {
			 lpht.put(i, 1.0*i);
		}
	}

	public void setUp2() {
		 lpht= new SeparateChainingHashTable<Integer,Double>();
	}
	
	public void testSize() throws Exception {
		setUp1();
		assertEquals("el elemento no es correto", 6, lpht.tamaño());
		setUp2();
		assertEquals("el elemento no es correto", 0, lpht.tamaño());
		
	}
	public void testPut() throws Exception {
		//Agrega un elemto sin superar la capacidad limite.
		setUp1();
		lpht.put(6, 6.0);
		//Agrega un elemento superando la capacidad y haciendo rehash.
		assertEquals("el elemento no es correto", 6.0, lpht.obtener(6));
		lpht.put(7, 7.0);
		assertEquals("el elemento no es correto", 7.0, lpht.obtener(7));
		setUp2();
		//Agrega un elemento en una lista vacia
		lpht.put(1, 1.0);
		assertEquals("el elemento no es correto", 1.0, lpht.obtener(1));
	}
	
	public void testGet() throws Exception {
		setUp1();
		assertEquals("el elemento no es correto", 4.0, lpht.obtener(4));
		assertEquals("el elemento no es correto", null, lpht.obtener(8));
		
	}

	
	

}
