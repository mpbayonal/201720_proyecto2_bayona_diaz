package model.data_structures;

import java.util.Arrays;

public class PriorityQueueMax <T extends Comparable<T>> 
{
	public PriorityQueueMax() 
	{
		tamano = 0;
		tamanoArreglo = 500;
		arreglo = (T[]) new Comparable[tamanoArreglo];

	}

	private int tamano;// numero de elementos en la cola
	private T[] arreglo;
	private int tamanoArreglo;// tamaño con que se creo el arreglo

	public T darElementoMayor() 
	{
		T elemento = null;

		if (tamano != 0) 
		{
			elemento = arreglo[1];
		}

		return elemento;

	}
	


	public T dequeueElementoMayor() 
	{
		T elemento = null;

		if (tamano != 0) 
		{
			elemento = arreglo[1];
			intercambiar(1, tamano);
			tamano--;
			sink(1);		

		}



		return elemento;

	}
	
	public void enqueue(T elemento) 
	{
		if(tamano >= tamanoArreglo-1) 
		{
			aumentarTamanoArreglo();
		}
		tamano++;
		arreglo[tamano] = elemento;
		swim(tamano);
		
	}

	
	
	
	public void sink(int i) 
	{
		int indice = i;
		while(indice * 2  <= tamano) 
		{
			int hijo = 2*indice;
			if( hijo < tamano &&  esMenor(hijo, hijo+1)) 
			{
				hijo++;
			}
			if( !esMenor(indice, hijo)) 
			{
				break;
			}
			
			intercambiar(indice, hijo);
			indice = hijo;
			
		}
	}
	
	public void swim(int i)
	{
		int indice = i;
		T elemento= arreglo[tamano];
		while(indice > 1 && esMenor(indice/2, indice))
		{
			intercambiar(indice, indice/2);
			indice =  indice/2;
		}
	}

	
	public void aumentarTamanoArreglo() 
	{		
		arreglo = Arrays.copyOf(arreglo, arreglo.length*2);
		tamanoArreglo = arreglo.length;		
	}


	public boolean esRaiz(int indice) 
	{
		
		return !(indice > 1);
	}

	public boolean tieneHijoIzquierdo(int indice) 
	{
		return indice*2 < tamano;
		
	}



	public void intercambiar(int index1 , int index2) 
	{
		T elemento = arreglo[index2];
		arreglo[index2] = arreglo[index1];
		arreglo[index1] = elemento;


	}

	public boolean esMenor(int indice1, int indice2)
	{
		return arreglo[indice1].compareTo(arreglo[indice2]) < 0;  

	}

	public boolean isEmpty()
	{
		return tamano == 0;
	}
	
	

	public int getSize()
	{
		return tamano;
	}
	
	
}

