package test;

import junit.framework.TestCase;
import model.data_structures.NodoListaDoblementeEncadenada;

public class NodoListaDoblementeEncadenadaTest extends TestCase 
{

	NodoListaDoblementeEncadenada<String> nodo;
	NodoListaDoblementeEncadenada<String> nodo1;
	NodoListaDoblementeEncadenada<String> nodo2;

	protected void setUp() throws Exception {
		nodo = new NodoListaDoblementeEncadenada<String>("Arbol");
		nodo1 = new NodoListaDoblementeEncadenada<String>("Hoja");
		nodo2 = new NodoListaDoblementeEncadenada<String>("tallo");
		nodo.cambiarAnterior(nodo1);
		nodo.cambiarSiguiente(nodo2);
	}


//	public void testNodoListaDoblementeEncadenada() {
//		fail("Not yet implemented");
//	}
	
	public void testDarElemento() {
		assertEquals("Debe debolver el elemento correcto ", "Arbol", nodo.darElemento());
	}

	public void testDarAnterior() {
		assertEquals("Debe debolver el elemento correcto ", "Hoja", nodo.darAnterior().darElemento());
	}

	public void testDarSiguiente() {
		assertEquals("Debe debolver el elemento correcto ", "tallo", nodo.darSiguiente().darElemento());
	}

	public void testCambiarSiguiente() {
		NodoListaDoblementeEncadenada<String> nuevo = new NodoListaDoblementeEncadenada<String>("rama");
		nodo.cambiarSiguiente(nuevo);
		assertEquals("Debe debolver el elemento correcto ", "rama", nodo.darSiguiente().darElemento());
	}

	public void testCambiarAnterior() {
		NodoListaDoblementeEncadenada<String> nuevo = new NodoListaDoblementeEncadenada<String>("flor");
		nodo.cambiarAnterior(nuevo);
		assertEquals("Debe debolver el elemento correcto ", "flor", nodo.darAnterior().darElemento());
	}

	

}
