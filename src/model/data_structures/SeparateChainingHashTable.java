package model.data_structures;

public class SeparateChainingHashTable<K extends Comparable<K>, V> 
{
	private int tamano;
	private int m;
	private DoubleLinkedListSymbolTable<K, V>[] tabla;
	
	public SeparateChainingHashTable( ) 
	{
		
		
		this.m = 11;
		tabla = (DoubleLinkedListSymbolTable<K, V>[]) new DoubleLinkedListSymbolTable[m];
		for (int i = 0; i < tabla.length; i++) 
		{
			tabla [i] = new DoubleLinkedListSymbolTable<K, V>();
		}
		
		
	}
	
	
	
	public void cambiarTamaño(int nuevoTamaño) throws Exception 
	{
		SeparateChainingHashTable<K, V> nuevo = new SeparateChainingHashTable<K, V>();
		for (int i = 0; i < tabla.length; i++) 
		{
			DoubleLinkedListSymbolTable<K, V> lista = tabla[i];
			
			HashNode<K, V> elemento = lista.next();
			while (elemento!= null) 
			{
			nuevo.put(elemento.darLlave(), elemento.getValue());
			elemento = lista.next();
			}
			
		}
		
		this.tamano = nuevo.tamano;
		this.tabla = nuevo.tabla;
		this.m = nuevo.m;
		
		
	}
	
	public void put(K llave, V valor) throws Exception
	{
		
		if(valor == null) 
		{
			borrar(llave);
		}
		else {
		if((tamano / m) >= 6) 
		{
			cambiarTamaño(2*m);
		}
		
		int indice = hash(llave);
		
		tabla[indice].add(valor, llave);
		tamano++;
		}
		
		
	}
	
	public boolean contiene(K llave) throws Exception 
	{
		return tabla[hash(llave)].existElement(llave);
	}
	
	public V obtener (K llave) throws Exception 
	{
		if(llave == null)throw new Exception();
		return tabla[hash(llave)].findElement(llave);
	}
	
	public void borrar(K llave) throws Exception 
	{	
		
		if(contiene(llave)) 
		{
			int posicion = hash(llave);
			tabla[posicion].delete(llave);
			if(tabla[posicion].getSize() == 0)tamano--;
		}
		
		
	}
	
	public int hash(K llave) throws Exception 
	{	if(llave == null)throw new Exception();
		return llave.hashCode() % m;
	}
	
	public int tamaño() 
	{
		return tamano;
	}
	
	 public Iterable<K> llaves() {
	        QueueIterable<K> cola = new QueueIterable<K>();
	        for (int i = 0; i < tabla.length; i++) 
			{
				DoubleLinkedListSymbolTable<K, V> lista = tabla[i];
				
				HashNode<K, V> elemento = lista.next();
				while (elemento!= null) 
				{
				cola.enqueue(elemento.darLlave());
				elemento = lista.next();
				}
				
			}
	        return cola;
	    } 
	
}
