package test;

import java.io.File;
import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.logic.PersistenceManager;
import model.logic.STSManager;
import model.vo.VOBusUpdate;

public class PersistenceManagerTest extends TestCase {

	private PersistenceManager persistenceManager;

	protected void setUp() throws Exception {
		persistenceManager = new PersistenceManager();
	}



	public void testLoadRoutes() throws Exception {

		assertEquals("Se cargaron los elementos del archivo Routes", persistenceManager.loadRoutes("./data/test/routes_test.txt").getSize().intValue(), 246);
	}

	public void testLoadTrips() throws Exception {


		assertEquals("Se cargaron los elementos del archivo Trips", persistenceManager.loadTrips("./data/test/trips_test.txt").getSize().intValue(), 1050);
	}

	public void testLoadStopTimes() throws Exception {

		assertEquals("Se cargaron los elementos del archivo StopTimes", persistenceManager.loadStopTimes("./data/test/stop_times_test.txt").getSize().intValue() , 2283);
	}

	public void testLoadStops() throws Exception {


		assertEquals("Se cargaron los elementos del archivo Stops", persistenceManager.loadStops("./data/test/stops_test.txt").getSize().intValue(), 1009);
	}

	public void testLoadAgencies() throws Exception {


		assertEquals("Se cargaron los elementos del archivo agency", persistenceManager.loadAgencies("./data/test/agency.txt").getSize().intValue(), 3);
	}

	public void testLoadEspecialServices() throws Exception {


		assertEquals("Se cargaron los elementos del archivo calendar_dates", persistenceManager.loadEspecialServices("./data/test/calendar_dates.txt").getSize().intValue(), 25);

	}

	public void testLoadServices() throws Exception {


		assertEquals("Se cargaron los elementos del archivo calendar", persistenceManager.loadServices("./data/test/calendar.txt").getSize().intValue(), 17);

	}

	public void testReadStopsEstimateService() throws Exception {

		assertEquals("Se cargaron los elementos del archivo stop estimate", 8, persistenceManager.readStopsEstimateService("data/test/stop estimate").getSize().intValue());
	}

	public void testReadBusUpdate() throws Exception {


		assertEquals("Se cargaron los elementos del archivo readBusUpdate", persistenceManager.readBusUpdate("data/test/buses service").getSize(), 2954);

	}

	public void testLoadTransfers() throws Exception {


		assertEquals("Se cargaron los elementos del archivo readBusUpdate", persistenceManager.loadTransfers("./data/test/transfers.txt").getSize().intValue(), 26);

	}
	
	public void testLoadShapes() throws Exception {


		assertEquals("Se cargaron los elementos del archivo shapes", persistenceManager.loadShapes("./data/test/shapes.txt").getSize().intValue(), 9);

	}
	public void testLoadInfo() throws Exception {


		assertEquals("Se cargaron los elementos del archivo shapes", persistenceManager.loadInfo("./data/test/feed_info.txt").getSize().intValue(), 1);

	}

}
